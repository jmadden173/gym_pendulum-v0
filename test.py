#!/usr/bin/python3

import os
import gym
import numpy as np

env = gym.make('Pendulum-v0')

env.reset()

outputarray = []

#for _ in range(200):
while True:
    #sample = env.action_space.sample()
    sample = [1.0]
    observation, reward, done, info = env.step(sample)

    outputarray.append(sample[0])

    print("Action: {}".format(sample))
    print("Observation: {}".format(observation))
    print("Reward: {}".format(reward))
    print("Info: {}".format(info))

    env.render()
    if done:
        env.reset()

        output_min = np.amin(outputarray)
        print("Min Values: {}".format(output_min))

        output_max = np.amax(outputarray)
        print("Max Values: {}".format(output_max))

        print("\n--------------- RESET ---------------\n")
        input("Press enter...")

env.close()
